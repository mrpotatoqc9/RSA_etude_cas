"""
#encription 8 bits
#clef privé (n,inverse ) inverse = e modulo z
#clef publique (n, e)  e =prime entre 3 et z
"""
import random
def generateur_de_nb_premier_liste(n): #genere une liste de nombre premier de 2 a n
	liste = []
	for nombre in range(2,n):
		j = (nombre/2) + 1 # : x/(x/2+1) < 2 ; 2 etant le premier diviseur autre que 1 et x
		j = int(j)
		while nombre%j != 0:
			if j == 2:
				liste += [nombre]
			j -= 1

		if nombre == 2:
			liste += [nombre]
		
	return liste
#liste_nb_premier = generateur_de_nb_premier_liste(29)
#print(liste_nb_premier)


def trouver_inverse_de_e(exposant_cripteur,z):
	for exposant_inverse in range(z):
		x = (exposant_cripteur*exposant_inverse) % z
		if x == 1:
			return exposant_inverse
	
def get_prime_number_between(p,q):
	#e >= p && e<q   e ne peut etre egal a q
	liste_nb_premier = generateur_de_nb_premier_liste(q-1)
	for i in range(len(liste_nb_premier)):
		if liste_nb_premier[i] == p:
			nouvelle_liste_premier = liste_nb_premier[i:len(liste_nb_premier)]
	#print(nouvelle_liste_premier)
	liste_size = len(nouvelle_liste_premier)
	random_index = random.randint(0,liste_size-1)
	exposant_cripteur = nouvelle_liste_premier[random_index]
	return exposant_cripteur
#print(get_prime_number_between(7,31))

#p & q devraient etre des nombres premiers aleatoires
def gen_pair_de_clef(p,q): #return clef privee:(n, inverse)  	clef_publique(
	n=p*q
	z = (p-1)*(q-1)
	exposant_cripteur = get_prime_number_between(p,q)
	inverse = trouver_inverse_de_e(exposant_cripteur,z)
	clef_privee = [n,inverse] #exposant decripteur
	clef_publique = [n,exposant_cripteur]
	
	return clef_privee,clef_publique
	
#clef_privee,clef_publique = gen_pair_de_clef(7,19)

def encription(message_a_cripte,clef_publique):
	n=clef_publique[0]
	exposant_cripteur = clef_publique[1]
	message_cripte=""
	for position_lettre in range(len(message_a_cripte)):
		#print(s[i])
		x = ord(message_a_cripte[position_lettre])#convertir acssi en int
		#print(x)
		x = pow(x,exposant_cripteur)
		#print ("x %n : ",x%n)
		x = chr(x%n) #reconverti en ascii
		#print("lettre",x)
		message_cripte += x 	 


	return message_cripte
	
#message_a_cripte = "secret"

#message_cripte = encription(message_a_cripte,clef_publique)

def message_a_decripte(message_cripte,clef_privee):
	n = clef_privee[0]
	inverse = clef_privee[1]#exposant inverse / exposant decripteur
	message_original=""
	for i in range(len(message_cripte)):
		x = ord(message_cripte[i])
		x = pow(x,inverse)
		x = x % n
		message_original += chr(x)

	return message_original

#message_orginal = message_a_decripte(message_cripte,clef_privee)

def verificateur_de_nb_premier(nombre): # retourne vrai si le nombre est premier
	if nombre == 1:
		return False
	i = (nombre/2)+1 # +1 pour que 2 retourne vrai
	i = int(i)
	if i == 2:
		return True
	while nombre%i != 0:
		if i == 2:
			return True
		i -= 1
	if i > 1:
		#print(i)
		return False
#print(verificateur_de_nb_premier(123456789))

def trouver_exposant_decripteur(clef_publique):
	n = clef_publique[0]
	e = clef_publique[1]  # exposant encrypteur
	naturel = []
	for i in range(2, n):
		if n % i == 0:
			# print(i)
			naturel.append(i)
	if len(naturel) < 2:
		return 1
	z = (naturel[0] - 1) * (naturel[1] - 1)
	inverse = trouver_inverse_de_e(e, z)
	#print("naturel: ",naturel)
	#print("exposant decrypteur:")
	#print(inverse)
	return inverse

#print(trouver_exposant_decripteur(":6`VOl46O7_0`6O`6+_6V6",(133,17)))




p = 7 # doit etre premier
q = 19# doit etre premier
clef_privee,clef_publique = gen_pair_de_clef(p,q)#p & q devraient etre des nombres premiers aleatoires
clef_privee_original = clef_privee
clef_publique_original = clef_publique
print ( "paire de clef:\n","clef privee:  ",clef_privee,"\n clef publique:",clef_publique)
message_a_cripte = "ceci est un message secret"
print("message_a_cripte:",message_a_cripte)
message_cripte = encription(message_a_cripte,clef_publique)
print("message cripte:",message_cripte)
message_original = message_a_decripte(message_cripte,clef_privee)
print("message original :", message_original)
print("\ntest breaking sans pub key \n")


prime = []
prime  = generateur_de_nb_premier_liste(256) # 2 a n

liste_n = []  # liste de tout les multiples de nombre premier de 2 a 256
for i in prime:
	for j in prime:
		k = i * j
		if k >= 256:
			break
		liste_n += [k]

# enlever les doublons dans la liste
new_liste = []
for N in liste_n:
	if N not in new_liste:
		new_liste.append(N)
liste_n = sorted(new_liste)
liste_n_size = len(liste_n)
# print(liste_n)

# copier le message cripte ici
index_liste_n = 0
liste_de_clef = []
mirror_message_cripte = ""

# tout les clef publique qui genere la meme chaine cripte
for index_liste_n in range(len(liste_n)):
	n = liste_n[index_liste_n]
	for exp in range(3, n):
		string_position = 0
		mirror_message_cripte = ""
		lettre = 32
		while lettre < 127:
			x = pow(lettre, exp)
			current_letter = chr(x % n)
			lettre += 1

			if current_letter == message_cripte[string_position]:
				mirror_message_cripte += current_letter
				string_position += 1
				lettre = 32
			if mirror_message_cripte == message_cripte:
				# print("\n EQUAL !!!!!!!!!!!")
				# print(mirror_message_cripte)
				clef_publique = (n, exp)
				# print("clef publique: ", clef_publique)
				liste_de_clef.append(clef_publique) #  liste de tous les clefs
				lettre = 127
				mirror_message_cripte = ""

new_liste = []
for clef in liste_de_clef:
	if clef not in new_liste:
		new_liste.append(clef)
liste_de_clef = sorted(new_liste)

print("le nombre de clef publique", len(liste_de_clef))
print("liste_de_clef", liste_de_clef)
set_de_clef = []
for clef_publique in liste_de_clef:
	# print("clef publique: ", clef_publique)
	inverse = trouver_exposant_decripteur(clef_publique)
	#print("exposant decrypteur: ",inverse)
	#print("n : ", clef_publique[0])
	if inverse == None:
		continue
	clef_privee = (clef_publique[0], inverse)
	s = message_a_decripte(message_cripte, clef_privee)
	#print("clef publique: ", clef_publique)
	#print("clef privee : ",clef_privee)
	#print("message secret: ",s)
	#print("\n")

	# pour les besoins de la cause on mets le string decripter ici
	if s == message_a_cripte:
		print("pour la chaine cripte: ",message_cripte," voici une combinaison possible :")
		print("! clef publique: ", clef_publique)
		print("! clef privee : ", clef_privee)
		print("! message secret: ", s)
		print("\n")
		set_de_clef.append(clef_privee)

print("la clef privee pour la clef publique :",clef_publique_original)
print(set_de_clef[0])

print("pair de clef original")
print("clef publique: ", clef_publique_original)
print("clef privee : ", clef_privee_original)