import RSA8bitsLib as rsa
# Breaking RSA 8 bits avec clef publique et message crypté
# clef publique : (133 , 31) -> (n,e)
# 31 est le modulo inverse de 7
# on cherche d entre 0 et n.  d = 7 dans ce cas ci

prime = []
prime  = rsa.generateur_de_nb_premier_liste(256) # 2 a n

liste_n = []  # liste de tout les multiples de nombre premier de 2 a 256
for i in prime:
	for j in prime:
		k = i * j
		if k >= 256:
			break
		liste_n += [k]

# enlever les doublons dans la liste
new_liste = []
for N in liste_n:
	if N not in new_liste:
		new_liste.append(N)
liste_n = sorted(new_liste)
liste_n_size = len(liste_n)
# print(liste_n)

# copier le message cripte ici
index_liste_n = 0
liste_de_clef = []
mirror_message_cripte = ""
message_cripte = "+6+O6`VOl4OH6``06O`6+_6V"
# tout les clef publique qui genere la meme chaine cripte
for index_liste_n in range(len(liste_n)):
	n = liste_n[index_liste_n]
	for exp in range(3, n):
		string_position = 0
		mirror_message_cripte = ""
		lettre = 32
		while lettre < 127:
			x = pow(lettre, exp)
			current_letter = chr(x % n)
			lettre += 1

			if current_letter == message_cripte[string_position]:
				mirror_message_cripte += current_letter
				string_position += 1
				lettre = 32
			if mirror_message_cripte == message_cripte:
				# print("\n EQUAL !!!!!!!!!!!")
				# print(mirror_message_cripte)
				clef_publique = (n, exp)
				# print("clef publique: ", clef_publique)
				liste_de_clef.append(clef_publique) #  liste de tous les clefs
				lettre = 127
				mirror_message_cripte = ""

new_liste = []
for clef in liste_de_clef:
	if clef not in new_liste:
		new_liste.append(clef)
liste_de_clef = sorted(new_liste)

print("le nombre de clef publique", len(liste_de_clef))
print("liste_de_clef", liste_de_clef)
set_de_clef = []
for clef_publique in liste_de_clef:
	# print("clef publique: ", clef_publique)
	inverse = rsa.trouver_exposant_decripteur(clef_publique)
	#print("exposant decrypteur: ",inverse)
	#print("n : ", clef_publique[0])
	if inverse == None:
		continue
	clef_privee = (clef_publique[0], inverse)
	s = rsa.message_a_decripte(message_cripte, clef_privee)
	#print("clef publique: ", clef_publique)
	#print("clef privee : ",clef_privee)
	#print("message secret: ",s)
	#print("\n")

	# pour les besoins de la cause on mets le string decripter ici
	if s == "ceci est un message secret":
		print("pour la chaine cripte: ",message_cripte," voici une combinaison possible :")
		print("! clef publique: ", clef_publique)
		print("! clef privee : ", clef_privee)
		print("! message secret: ", s)
		print("\n")
		set_de_clef.append(clef_privee)


print("clef de decription, la clef privee:", set_de_clef[0])


