import RSA8bitsLib as rsa
# Breaking RSA 8 bits avec clef publique et message crypté
# clef publique : (133 , 17) -> (n,e)
# 31 est le modulo inverse de 7
# on cherche d entre 0 et n.  d = 7 dans ce cas ci

s2 = ":6`VOl46O7_0`6O`6+_6V6"
clef_publique = (133, 17)
inverse = rsa.trouver_exposant_decripteur(clef_publique)
print("exposant decrypteur:")
print(inverse)

clef_privee = (clef_publique[0],inverse)
s = rsa.message_a_decripte(s2, clef_privee)
print("message secret:")
print(s)
