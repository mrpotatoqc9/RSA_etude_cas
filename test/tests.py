import unittest
import os, sys
root = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append(root)
import RSA8bitsLib as rsa

class LibTest(unittest.TestCase):
	def test_generateur_de_nb_premier_liste(self):
		x = rsa.generateur_de_nb_premier_liste(6)
		val = [2,3,5]
		self.assertTrue(val == x, x)
		val = [2,3]
		self.assertFalse(val == x, x)

	def test_pipeline(self):
		p = 7 # doit etre premier
		q = 19# doit etre premier
		clef_privee, clef_publique = rsa.gen_pair_de_clef(p,q)#p & q devraient etre des nombres premiers aleatoires
		self.assertTrue(len(clef_publique) == 2, clef_publique)
		self.assertTrue(type(clef_privee[0]) == type(1), clef_publique)
		message_a_cripte = "secret"
		message_cripte = rsa.encription(message_a_cripte,clef_publique)
		message_original = rsa.message_a_decripte(message_cripte,clef_privee)
		self.assertTrue(message_a_cripte == message_original, message_original)

if __name__ == '__main__':
	unittest.main()

