# encription 8 bits
# clef privé (n,inverse ) inverse = e modulo z
# clef publique (n, e)  e =prime entre 3 et z
import random


def generateur_de_nb_premier_liste(n:int) -> list :  # genere une liste de nombre premier de 2 a n
	liste = []
	for nombre in range(2, n):
		j = (nombre/2) + 1  # : x/(x/2+1) < 2 ; 2 etant le premier diviseur autre que 1 et x
		j = int(j)
		while nombre % j != 0:
			if j == 2:
				liste += [nombre]
			j -= 1

		if nombre == 2:
			liste += [nombre]
		
	return liste
#liste_nb_premier = generateur_de_nb_premier_liste(29)
#print(liste_nb_premier)


def trouver_inverse_de_e(exposant_cripteur, z):
	for exposant_inverse in range(z):
		x = (exposant_cripteur*exposant_inverse) % z
		if x == 1:
			return exposant_inverse
	

def get_prime_number_between(p, q):
	#e >= p && e<q   e ne peut etre egal a q
	liste_nb_premier = generateur_de_nb_premier_liste(q-1)
	for i in range(len(liste_nb_premier)):
		if liste_nb_premier[i] == p:
			nouvelle_liste_premier = liste_nb_premier[i:len(liste_nb_premier)]
	#print(nouvelle_liste_premier)
	liste_size = len(nouvelle_liste_premier)
	random_index = random.randint(0, liste_size-1)
	exposant_cripteur = nouvelle_liste_premier[random_index]
	return exposant_cripteur
#print(get_prime_number_between(7,31))


# p & q devraient etre des nombres premiers aleatoires entre 3 et 255
def gen_pair_de_clef(p, q):  #return clef privee:(n, inverse)  	clef_publique(n,exposant_cripteur)
	n = p*q
	z = (p-1)*(q-1)
	exposant_cripteur = get_prime_number_between(p, q)
	inverse = trouver_inverse_de_e(exposant_cripteur, z)
	clef_privee = [n, inverse]  # exposant decripteur
	clef_publique = [n, exposant_cripteur]
	
	return clef_privee, clef_publique
	
#clef_privee,clef_publique = gen_pair_de_clef(7,19)


def encription(message_a_cripte, clef_publique):
	n = clef_publique[0]
	exposant_cripteur = clef_publique[1]
	message_cripte = ""
	for position_lettre in range(len(message_a_cripte)):
		#print(s[i])
		x = ord(message_a_cripte[position_lettre])  # convertir acssi en int
		#print(x)
		x = pow(x, exposant_cripteur)
		#print ("x %n : ",x%n)
		x = chr(x % n)  # reconverti en ascii
		#print("lettre",x)
		message_cripte += x
	return message_cripte
	
#message_a_cripte = "secret"
#message_cripte = encription(message_a_cripte,clef_publique)


def message_a_decripte(message_cripte, clef_privee):
	n = clef_privee[0]
	inverse = clef_privee[1]  # exposant inverse / exposant decripteur
	message_original = ""
	for i in range(len(message_cripte)):
		x = ord(message_cripte[i])
		x = pow(x, inverse)
		x = x % n
		message_original += chr(x)

	return message_original

#message_orginal = message_a_decripte(message_cripte,clef_privee)


def verificateur_de_nb_premier(nombre):  # retourne vrai si le nombre est premier
	if nombre == 1:
		return False
	i = (nombre / 2) + 1  # +1 pour que 2 retourne vrai
	i = int(i)
	if i == 2:
		return True
	while nombre % i != 0:
		if i == 2:
			return True
		i -= 1
	if i > 1:
		#print(i)
		return False
#print(verificateur_de_nb_premier(123456789))


def trouver_exposant_decripteur(clef_publique):
	n = clef_publique[0]
	e = clef_publique[1]  # exposant encrypteur
	naturel = []
	for i in range(2, n):
		if n % i == 0:
			# print(i)
			naturel.append(i)
	if len(naturel) < 2:
		return 1
	z = (naturel[0] - 1) * (naturel[1] - 1)
	inverse = trouver_inverse_de_e(e, z)
	#print("naturel: ",naturel)
	#print("exposant decrypteur:")
	#print(inverse)
	return inverse


"""
p = 7 # doit etre premier
q = 19# doit etre premier
clef_privee,clef_publique = gen_pair_de_clef(p,q)#p & q devraient etre des nombres premiers aleatoires
print ( "paire de clef:\n","clef privee:  ",clef_privee,"\n clef publique:",clef_publique)
message_a_cripte = "secret"
print("message_a_cripte:",message_a_cripte)
message_cripte = encription(message_a_cripte,clef_publique)
print("message cripte:",message_cripte)
message_original = message_a_decripte(message_cripte,clef_privee)
print("message original :", message_original)
"""
